# ECD Smart Agriculture Competition Team 2 Robot Software

This is the program for the control system of Team 2's entry into the
Emirates-Coventry-Deakin Smart Agriculture Competition.

## Program Architecture Overview
This program is broken up into three sections for ease of programming:
 - Sensors
 - Scheduler
    - Super goal
    - State Machine (Goals)
    - Sub-State Machine (Sub-Goals)
 - Effectors

We will now explore each of these three layers (and their sub-layers) in detail
below.

### Sensors
The sensors component of the program takes the raw information from the sensor
inputs (such as the Time-of-Flight sensors, IR sensors, motor encoders and Pixy
  computer-vision system) and processes this into information that is readily
  usable by the scheduler for basing decisions of scheduler, with the aim of
  removing obvious errors, outliers, false-positives and other anomalies from
  the input data set, as well as formatting the input into appropriate variables
  to make the programming and operation of the scheduler simpler. Removing
  complexity of the scheduler is a main aim of the sensor layer, as the
  scheduler is easily the most complex part of the program and any steps to
  reduce the complexity of this layer will aid and assist in debugging and
  programming this part of the program. A secondary part of the sensor layer is
  to identify any important input data, like IR sensors indicating that the
  robot has left the active area, and flag this sensor input as important so
  that the scheduler can immediately start processing this data first as a
  matter of urgency, to determine if any actions need to be made immediately.

### Scheduler
The scheduler is the main part of the program, that makes all the important
decisions. The scheduler is broken up into smaller sections that each have
defined purposes. The basis of programming the scheduler is state machines, of
which the scheduler has many. The scheduler uses a "goal based" approach to
completing its task, by identifying goals and completing them. The scheduler
contains functions for competing each goal, as well as a decision making process
for determining which goals should be completed in which order depending on
input information from the sensor layer.

#### Super-State Machine
This overseeing layer is the main component of the scheduler, but it doesn't do
much when not required. It serves to identify problems and rectify them
immediately, interrupting the operation of the main state machine in the
process. If there is no need for immediate action by the super-state machine, it
calls the main state machine process.

#### Main State machine
This is the main layer of the scheduler, that makes all the significant and
ordinary decisions of the scheduler, such as which goals to peruse, as well as
making judgements about which order to complete goals in, as well as determining
when they have been completed.

#### Sub-State Machines (Secondary State Machines)
The Sub-State machine (or secondary state machines) are state machines that sit
inside of the main state machine, and are used to logically break up the
execution of large goals/steps into manageable tasks, for ease of programming as
well as execution and debug. Each sub-state machine should be its own function.

### Effectors
The effectors, physically, are the parts of the machine capable of providing
output, such as motors. From a programming point of view, the effector layer
of the program is responsible for activating/deactivating the effectors of the
robot as required by the scheduler. The effector layer takes in input like a
desired speed, direction or distance, or combination of all three, and uses
methods to action this, for example, a PID controller.

### Controller
A hidden and fourth 'layer' of the program design is the controller, which does
nothing in normal circumstances, but if debug is enabled, creates an interactive
command line interface (CLI) over the serial terminals to allow engineers to
modify the operating parameters of the robot on-the-fly for debugging as well as
testing purposes.

## Compiling and Running
This program has been created in Microsoft Visual Studio 2017, with the Visual
Micro extension, however, any IDE capable of compiling for and uploading to a
Teensy 3.5 should be capable of editing, compiling and uploading this program.
(Including the Arduino IDE with Teensy programmer addon).

The program is designed to run on a Teensy 3.5, with specific additional
hardware (such as motors and sensors). This README document seeks to outline the
architecture of the program.

## Credits
This program has been created by the ECD Team 2:
 - Alexander Gregory _(Deakin University)_
 - Ayoub Aldam _(Coventry University)_
 - Levi Matthysen _(Emirates Aviation University)_
 - Majed Alhassan _(Coventry University)_
 - Mohammed GulamHusen Patel _(Emirates Aviation University)_
 - Lachlan Stewart _(Deakin University)_
