/*
 Name:      ECD_Team_2_Program.ino
 Created:   2019-22-03 12:24:47 PM
 Version:   0.0.1
 Authors:   Alexander Gregory, Lachlan Stewart, Mohammed GulamHusen Patel,
			  Levi Matthysen, Majed Alhassan, Ayoub Aldam
 About:     This is the AI program / robot controller designed to operate our
			  entry for the ECD Smart Agriculture Competition for Team 2 in
			  2019. For more information refer to the README.md. This is
			  designed to complie to and operate on a Teensy 3.5, and its
			  operation on other platforms is not endorsed or garunteed.
Copyright:  The copyright for this software belongs to it's respective owners.
			  (c) 2019.
*/

// Uncomment the following line to disable the pixy library and its functions.
//  You may wish to do this if you need to run the program with the pixy
//  unplugged, as the pixy library will halt the program during pixy.init() if
//  it can't establish communication with the pixy.
//#define PIXY_DISABLE

// Pin definitions
#define PIN_I2C_SCL 19
#define PIN_I2C_SDA 18
#define PIN_SPI_MOSI 0
#define PIN_SPI_MISO 1
#define PIN_SPI_SS 31
#define PIN_SPI_SCK 32

#define PIN_LED_GREEN A21
#define PIN_LED_RED A22

#define PIN_MOTOR_LEFT_PWM 7
#define PIN_MOTOR_LEFT_DIR 8
#define PIN_MOTOR_LEFT_POW 9
#define PIN_MOTOR_RIGHT_PWM 23
#define PIN_MOTOR_RIGHT_DIR 22
#define PIN_MOTOR_RIGHT_POW 21

#define PIN_ENCODER_LEFT_INT 5
#define PIN_ENCODER_LEFT_DIR 6
#define PIN_ENCODER_RIGHT_INT 17
#define PIN_ENCODER_RIGHT_DIR 20

#define PIN_SWITCH 24

#define PIN_TOF_LEFT 16
#define PIN_TOF_RIGHT 35

#define PIN_IR_F_O 37
#define PIN_IR_F_L 14
#define PIN_IR_F_R 39
#define PIN_IR_B_O 36
#define PIN_IR_B_L 15
#define PIN_IR_B_R 38
constexpr uint8_t IR_INPUTS[6]{ PIN_IR_F_O,PIN_IR_F_L,PIN_IR_F_R,PIN_IR_B_O,PIN_IR_B_L,PIN_IR_B_R };

#define IR_SAMPLES 20
#define IR_TOLERANCE 75

#define PIXY_CHECK_WIDTH_MIN 10
#define PIXY_CHECK_WIDTH_MAX 150
#define PIXY_CHECK_HEIGHT_MIN 10
#define PIXY_CHECK_HEIGHT_MAX 200
#define PIXY_CHECK_AREA_MIN 750
#define PIXY_CHECK_AGE_MIN 10

#define PID_OUTPUT_MAX 255
#define PID_OUTPUT_MIN 0
#define PID_CONSTANT_P 11
#define PID_CONSTANT_I 2
#define PID_CONSTANT_D 11

#define SPEED_CRAWL 25
#define SPEED_SLOW 40
#define SPEED_MED 55
#define SPEED_FAST 65

#define ENCODER_CM 0.0412

// Include libraries
#include "src/Pixy2/Pixy2.h"    /// Library for the Pixy

// Create the pixpy camera link
#ifndef PIXY_DISABLE
Pixy2 pixy;
#endif

// Initialise variables
uint16_t state = 1;             /// Keeps track of the current state
uint16_t stateOld = 0;          /// Keeps track of the previous state
uint16_t stateSuper = 0;        /// Keeps track of the super-state (super-scheduler)
uint8_t stateCLI = '\0';        /// Keeps track of the current state of the CLI (see CONTROLLER section)

volatile int32_t encoderLeft = 0;        /// Keeps track of how many ticks seen on the left motor
volatile int32_t encoderRight = 0;       /// Keeps track of how many ticks seen on the right motor
int32_t encoderLeftTarget = 0;  /// For tracking how far the motors have travelled over time
int32_t encoderTarget = 0;      /// Used for single-wheel targeting when the old targets need to be preserved
int32_t encoderRightTarget = 0; /// For tracking how far the motors have travelled over time
int32_t encoderLeftLast = 0;    /// For calculating speed, the last position of the left encoder
int32_t encoderRightLast = 0;   /// For calculating speed, the last position of the right encoder
int32_t encoderTime = 0;        /// For tracking time when calculating speed
int16_t speedLeft;              /// Current speed of the left motor
int16_t speedRight;             /// Current speed of the right motor

float irThreshold[6];           /// Threshold value for the IR sensors black level.
bool irFlags[6];                /// Output value for the IR sensors current detection state
uint16_t irRaw[6];              /// Raw read value from the IR sensors

bool debug = false;             /// Used to handle debug output. Debug is only printed to serial if this is TRUE.
bool debugPixy = false;         /// Used to handle debug output specific to the pixy camera. Functions samne as above.

int16_t pixyX;                  /// Used to store the current distance from the center of the robot of the currently tracked object
uint16_t pixyY;                 /// Used to store the distance away from the robot of the currently tracked object
uint8_t pixyColour;             /// Used to track if the tracked object is a coloured cube (and if so, which one) or the finish area
uint8_t pixyIndex;              /// Represents the tracking ID of the curretnly tracked object.
int16_t baseX;                  /// Used to track where the base (green drop-off area) is.
int16_t baseY;                  /// Used to track where the base (green drop-off area) is.
uint32_t pixyUpdate;            /// Tracks the time since the last update

uint32_t timeOld = 0;           /// Used for tracking time for the sanity check.
uint8_t counterOne;             /// Used for some timing operations (see state 900)
uint32_t greenTime = 0;			/// Used for tracking the time waited before checking green

int PIDleft_lastError;          /// Last proportional error of the left motor
int PIDright_lastError;         /// Last proportional error of the right motor
int PIDleft_errorI;             /// Cumulative integral error for left motor
int PIDright_errorI;            /// Cumulative integral error for right motor
int PIDleft_setpoint;           /// Setpoint speed for left motor
int PIDright_setpoint;          /// Setpoint speed for right motor

int numBlocksFound = 0;         /// Number of blcoks found
int blockReadDelay = 12;        /// Time that an object must be tracked for to be considered a new object
int leeway = 50;                /// Distance that a block can be off by in pixels and still considered to be the same.
int reference_x = 0;           	/// Last known static X location
int reference_y = 0;          	/// Last known static Y location
int current_pos_x = 0;          /// Robot's current X location, based on the encoders and reference values
int current_pos_y = 0;          /// Robot's current Y location, based on the encoders and reference values
int red_x = 0;                  /// location of the detected red block
int red_y = 0;                  /// location of the detected red block
int red_blocks[2][3] = {{ -1, -1, -1}, { -1, -1, -1}}; //Location of the detected red blocks, -1 means none. Format: {x,x,x},{y,y,y}

bool moving_in_x = true;                        //True = moving in x axis, east-west, False = moving in y axis, north-south

bool headlight = false;

void setup()
{
	// This is the function that initialises everything.
	initialise();
}

void loop()
{
	// This is the main loop. It calls the three main sections of the program, as
	//   well as the controller, with enables interactivity over the bluetooth
	//   module for debug purposes.
	section_sensors();
	section_scheduler();
	section_effector();
	controller();
}

// FUNCTION:    initialise
// INPUTS:      nill
// OUTPUTS:     nill
// PURPOSE:     Ensure the robot is ready to run before it runs
void initialise() {
	// Open serial comms
	Serial.begin(115200);       /// Open serial communications to the debug console
	Serial5.begin(9600);        /// Open serial communications to the bluetooth module
	Serial.println("Program Starting");
	Serial5.println("");        /// Most serial consoles won't clear on a reset, so make sure we start on a fresh line.
	Serial5.println("Program Starting");

	// Set pin modes
	pinMode(PIN_MOTOR_LEFT_PWM, OUTPUT);
	pinMode(PIN_MOTOR_LEFT_DIR, OUTPUT);
	pinMode(PIN_MOTOR_LEFT_POW, OUTPUT);
	pinMode(PIN_MOTOR_RIGHT_PWM, OUTPUT);
	pinMode(PIN_MOTOR_RIGHT_DIR, OUTPUT);
	pinMode(PIN_MOTOR_RIGHT_POW, OUTPUT);

	pinMode(PIN_ENCODER_LEFT_INT, INPUT_PULLUP);
	pinMode(PIN_ENCODER_LEFT_DIR, INPUT_PULLUP);
	pinMode(PIN_ENCODER_RIGHT_INT, INPUT_PULLUP);
	pinMode(PIN_ENCODER_RIGHT_DIR, INPUT_PULLUP);

	pinMode(PIN_TOF_LEFT, INPUT);
	pinMode(PIN_TOF_RIGHT, INPUT);

	pinMode(PIN_IR_F_O, INPUT);
	pinMode(PIN_IR_F_L, INPUT);
	pinMode(PIN_IR_F_R, INPUT);
	pinMode(PIN_IR_B_O, INPUT);
	pinMode(PIN_IR_B_L, INPUT);
	pinMode(PIN_IR_B_R, INPUT);

	pinMode(LED_BUILTIN, OUTPUT);
	pinMode(PIN_LED_GREEN, OUTPUT);
	pinMode(PIN_LED_RED, OUTPUT);
	analogWrite(PIN_LED_GREEN, LOW);
	analogWrite(PIN_LED_RED, LOW);

	pinMode(PIN_SWITCH, INPUT);

	// Begin SPI connection
	SPI1.setMOSI(PIN_SPI_MOSI);
	SPI1.setMISO(PIN_SPI_MISO);
	SPI1.setSCK(PIN_SPI_SCK);
	SPI1.begin();

	// Initialise the pixy camera
#ifndef PIXY_DISABLE
	pixy.init();                /// Establish connection to the pixy CMUcam5
	pixy.setLamp(0, 0);         /// Turn the headlights off
	pixy.setLED(255, 0, 0);     /// Set LED to RED to indicate "not ready"
	pixy.getResolution();       /// This is needed to populate frameWidth and frameHeight variables
#endif

  // Initialise the ToF sensors

  // Initialise the IR sensors
	initialise_ir(false);

	// Attach interrupts
	attachInterrupt(digitalPinToInterrupt(PIN_ENCODER_LEFT_INT), sensors_encoder_left, RISING);
	attachInterrupt(digitalPinToInterrupt(PIN_ENCODER_RIGHT_INT), sensors_encoder_right, RISING);

	// Indicate that the program is ready to go
#ifndef PIXY_DISABLE
	pixy.setLED(0, 0, 255);     /// Set LED to BLUE to indicate "ready and waiting"
#endif
	Serial.println("Initialisation Finished");
	Serial5.println("Initialisation Finished");
}

// FUNCTION:    initialise_ir
// INPUTS:      black
// OUTPUTS:     nill
// PURPOSE:     Sets a background value for the line sensors
void initialise_ir(bool debug) {
	/// This is needed to get a background reading
	for (uint8_t i = 0; i < IR_SAMPLES; i++) {
		// For however many samples, and then six, one for each sensor...
		for (uint8_t j = 0; j < 6; j++) {
			irThreshold[j] += analogRead(IR_INPUTS[j]);
		}
	}
	// Add a tolerance
	for (uint8_t k = 0; k < 6; k++) {
		irThreshold[k] = irThreshold[k] / IR_SAMPLES;
		irThreshold[k] -= IR_TOLERANCE;
		if (debug) Serial5.println(irThreshold[k]);
	}
}


///////////////////////////////// SENSORS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// FUNCTION:    section_sensors
// INPUTS:      nill
// OUTPUTS:     nill
// PURPOSE:     Polls sensors and modifies data into a useable form for the scheduler.
void section_sensors() {
	sensors_ir();
	sensors_pixy();
	sensors_encoder();
	sensors_position();
}

// FUNCTION:    sensors_pixy
// INPUTS:      raw array of detected objects from pixy camera
// OUTPUTS:     processed array of detected objects from pixy camera.
// PURPOSE:     This takes the raw array of detections from the pixy camera, and
//                removes any obvious anomolies (i.e.: blocks too large,
//                too small, wrong aspect ratio, etc.) and returns a sanitised
//                list for use by other parts of the program.
void sensors_pixy() {
#ifndef PIXY_DISABLE
	// The pixy update function takes a while to update, and the library is
	//  probably blocking, so we have this 'delay' in to only update it
	//  (or poll for latest updates) every 100ms at most frequent, so that it
	//  doesn't slow down the rest of the program, which has been causing the
	//  IR sensors to miss lines. This fixes that problem.
	if (millis() > pixyUpdate + 100) {
		pixyUpdate = millis();
		pixyColour = 0;
		uint16_t pixySize = 0;
		for (int i = 0; i < pixy.ccc.getBlocks(true, 7, 255); i++) {
			char buf[256];
			if (debugPixy) {
				/// Print debug information to serial if pixy debugging is enabled
				Serial5.print(i);
				sprintf(buf, " sig: %d x: %d y: %d width: %d height: %d index: %d age: %d", pixy.ccc.blocks[i].m_signature, pixy.ccc.blocks[i].m_x, pixy.ccc.blocks[i].m_y, pixy.ccc.blocks[i].m_width, pixy.ccc.blocks[i].m_height, pixy.ccc.blocks[i].m_index, pixy.ccc.blocks[i].m_age);
				Serial5.print(buf);
			}
			// These following if statements look for signs that the incoming blocks
			//  are false-positives and causes them to be removed. It checks that the
			//  width and height are within expected parameters, and that the cube has
			//  been detected for long enough (as most false-positives only last a few
			//  frames, so having a block detection for more than a few frames is
			//  probably a legitimate detection)
			if ((pixy.ccc.blocks[i].m_width < PIXY_CHECK_WIDTH_MIN) || (pixy.ccc.blocks[i].m_width > PIXY_CHECK_WIDTH_MAX)) {
				// This returns true if the width is too small or too large. We will ignore it, and skip to the next iteration of the foor loop.
				continue;
			}
			if ((pixy.ccc.blocks[i].m_height < PIXY_CHECK_HEIGHT_MIN) || (pixy.ccc.blocks[i].m_height > PIXY_CHECK_HEIGHT_MAX)) {
				// This retruns true if the height is too small or too large. We will ignore it, and skip to the next iteration of the for loop.
				continue;
			}
			uint16_t size = pixy.ccc.blocks[i].m_width * pixy.ccc.blocks[i].m_height;
			if (size < PIXY_CHECK_AREA_MIN) {
				// This returns true if the area of the object is too small. We will ignore it, and skip to the next iteration of the for loop.
				continue;
			}
			if (pixy.ccc.blocks[i].m_age < PIXY_CHECK_AGE_MIN) {
				// This returns true if the pixy hasn't seen a given object for at least n frames. This is used to remove "noise".
				continue;
			}
			if (pixyColour != 0 && pixyColour != pixy.ccc.blocks[i].m_signature) {
				// If PixyColour is not zero, then we are currently tracking something, so only update the vars if its already the same colour.
				continue;
			}

			// If we have reached this far, then we have a (hopefully) valid input.
			switch (pixy.ccc.blocks[i].m_signature) {
			// Is this a red block/cube? In which case store this in a special place and calculate the location to transmit to the end-user.
			case 1:
				//absolute position determined by block location and robot location
				red_x = (pixy.ccc.blocks[i].m_x + current_pos_x) * ENCODER_CM;
				red_y = (pixy.ccc.blocks[i].m_y + current_pos_y) * ENCODER_CM;
				break;

			// Is this the base? In which case store this in a special place for the base information
			case 2:
				// We are looking at something green
				baseX = pixy.ccc.blocks[i].m_x - (pixy.frameWidth / 2);
				baseY = pixy.frameHeight - (pixy.ccc.blocks[i].m_y + (pixy.ccc.blocks[i].m_height / 2));
				pixyColour = 2;
				// No need to evaluate the rest of the options, so we shall continue/break
				break;

			// Is this the blue cube? In which case store this in a special place for the cube collecting function
			case 3:
				// We are looking at something blue
				pixyX = pixy.ccc.blocks[i].m_x - (pixy.frameWidth / 2);
				pixyY = pixy.frameHeight - (pixy.ccc.blocks[i].m_y + (pixy.ccc.blocks[i].m_height / 2));
				// Store the size of this object in something more permanent, because we only want to know about the largest.
				pixySize = size;
				pixyColour = 3;
				pixyIndex = pixy.ccc.blocks[i].m_index;
				// No need to evaluate the rest of the options, so we shall continue
				break;

			default:
				// We don't know what this detection is, so we will ignore it.
				break;
			}
		}
		if (debugPixy) display_pixy();
	}
#endif
}

// FUNCTION:    sensors_ir
// INPUTS:      raw input from IR sensors
// OUTPUTS:     boolean states of detected colour
// PURPOSE:     This takes in the raw analogue inputs from the 6 IR sensors and
//                tries to determine if it is looking at the black course area
//                (or the green base/starting area) or the white forbidden area,
//                and return this information as discrete information for the
//                rest of the program to use.
// NOTES:       The detection of white is a significant problem, as the robot
//                should not enter any white areas, and this state should cause
//                immediate action by the scheduler.
void sensors_ir() {
	// Use a for loop to check each sensor and categorise its output into three simple categories
	for (int i = 0; i < 6; i++) {
		irRaw[i] = analogRead(IR_INPUTS[i]);
		if (irRaw[i] > irThreshold[i]) {
			// We are below the black threshold, so return false for black
			irFlags[i] = false;
		}
		else {
			// We are above the black threshold, so return true for white.
			irFlags[i] = true;
		}
	}
}

// FUNCTION:    sensors_encoder
// INPUTS:      interrupts from the encoders
// OUTPUTS:     speed, distance
// PURPOSE:     Takes the raw input from the encoder interrupts and uses them to
//                calculate speed and travelled distance from the raw input
void sensors_encoder() {
	// We only wish to run in reasonable amounts of time. Sample too often and the encoders probably haven't moved.
	if (encoderTime + 50 < millis()) {
		encoderTime = millis();
		// Calculate the speed (change since last update)
		speedLeft = -(encoderLeft - encoderLeftLast);
		speedRight = (encoderRight - encoderRightLast);
		// Store the current values for use later
		encoderLeftLast = encoderLeft;
		encoderRightLast = encoderRight;
		// This stores the speed in a global variable in terms of encoder ticks per 0.05 seconds.
	}
}

// FUNCTION:    sensors_encoder_left
// INPUTS:      nill
// OUTPUTS:     nill
// PURPOSE:     This is the interrupt service routine for the left encoder, and
//                is triggered when any pulse is deteced on the left encoder
//                input.
void sensors_encoder_left() {
	// Because there are two offset encoder disks, we can use whether the other
	//   one has also triggered to know which direction the wheel is moving in.
	if (digitalRead(PIN_ENCODER_LEFT_DIR) == HIGH) {
		encoderLeft--;
	}
	else {
		encoderLeft++;
	}
}

// FUNCTION:    sensors_encoder_right
// INPUTS:      nill
// OUTPUTS:     nill
// PURPOSE:     This is the interrupt service routine for the right encoder, and
//                is triggered when any pulse is detected on the right encoder
//                input.
void sensors_encoder_right() {
	if (digitalRead(PIN_ENCODER_RIGHT_DIR) == HIGH) {
		encoderRight--;
	}
	else {
		encoderRight++;
	}
}

//  Name: red_detected
//  Input:
//  Output: none
//  Note: Determines if the red block has already been detected and then saves or discards the information accordingly.
void sensors_position()
{
	if (moving_in_x)
	{
		// If moving in the x direction, encoder values indicate how far in x the robot has gone
		current_pos_x = reference_x + encoderLeft + encoderRight;
		current_pos_y = reference_y;
	}
	else
	{
		// Otherwise, assume movement in y direction
		current_pos_x = reference_x;
		current_pos_y = reference_y + encoderLeft + encoderRight;
	}
	//Positions are taken for the currently detected block
}


///////////////////////////////// SCHEDULER \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// FUNCTION:    section_scheduler
// INPUTS:      nill
// OUTPUTS:     nill
// PURPOSE:     This uses a state machine to determine if there are any critical
//                functions that need to be run, and if not, runs the main state
//                machine.
void section_scheduler() {
	schedule_blocks();
	sanity_check();
	switch (state) {
	case 0:
		// Case 0: Waiting to start:
		/// check if button is pressed. If not, do nothing
		pixy.setLED(0, 255, 255);
		motor_stop();
		break;

	case 1:
		// Case 1: Start the program.
		//   As required, the program needs to be started by a physical button (here
		//   PIN_SWITCH), which should start the program after a delay of 5 seconds
		//   when pressed.
		motor_stop();
		pixy.setLED(0, 255, 255);

		if (digitalRead(PIN_SWITCH) == LOW) {
			// THIS IS BLOCKING CODE!
			state = 9;
			encoderLeftTarget = encoderLeft + 500;
			encoderRightTarget = encoderRight + 500;

			Serial5.println("[STS] Started Manually");
			delay(500);
			Serial5.print("[STS] ");
			for (uint8_t i = 5; i > 0; i--) {
				Serial5.print(i);
				analogWrite(PIN_LED_GREEN, 255);
				analogWrite(PIN_LED_RED, 255);
				delay(250);
				Serial5.print(".");
				delay(250);
				Serial5.print(".");
				analogWrite(PIN_LED_GREEN, 0);
				analogWrite(PIN_LED_RED, 0);
				delay(250);
				Serial5.print(".");
				delay(250);
			}
			Serial5.println(" GO!");
			
			greenTime = millis();		//Start counting to see if green is a valid input
			pixy.setLamp(true, false);
			headlight = true;
		}
		reference_x = 0;
		reference_y = 0;
		break;

	case 9:
		// Drive forward out of green
		motor_drive(SPEED_FAST);
		if (encoderLeft > encoderLeftTarget && encoderRight > encoderRightTarget) {
			motor_stop();
			state = 10;
		}

	case 10:
		// Case 100: Follow the line up the right side of the course
		// As we are in "normal" mode, we will set the RGB LED white, and the status lights off
		pixy.setLED(255, 255, 255);
		analogWrite(PIN_LED_RED, 0);
		analogWrite(PIN_LED_GREEN, 0);

		// Start driving with a bias to one side, so we aren't in a straight line
		motor_single_wheel(false, SPEED_SLOW + 1);
		motor_single_wheel(true, SPEED_SLOW);

		// Check the IR sensors if we are over any lines
		if (irFlags[0] || irFlags[1]) {
			// We are on white, get OUT
			Serial5.println("[STS] Detected on LEFT ");
			state = 30;
		}

		if (irFlags[2]) {
			// We have hit the line, get OUT (the other way)
			Serial5.println("[STS] Detected on RIGHT");
			state = 20;
		}
		break;

	case 20:
		// We are too close to the edge, back up then continue
		pixy.setLED(255, 255, 0);
		analogWrite(PIN_LED_RED, 255);

		encoderLeftTarget = encoderLeft - 90;
		encoderRightTarget = encoderRight - 90;
		motor_drive(-SPEED_FAST);
		
		reference_x -= 90;
		state = 21;
		Serial5.println("[STS] State 21: Back away from perimiter");
		break;

	case 21:
		// Turn left a little one we have backed up enough
		if (encoderLeft < encoderLeftTarget && encoderRight < encoderRightTarget) {
			motor_stop();
			encoderLeftTarget = encoderLeft - 250;
			motor_single_wheel(false, -SPEED_MED);

			pixy.setLED(255, 128, 0);
			state = 22;
		}
		break;

	case 22:
		// Once we have turned far enough, go back to our old stuff.
		if (encoderLeft < encoderLeftTarget) {
			motor_stop();
			state = 10;
			pixy.setLED(255, 0, 0);
		}
		break;

	case 30:
		// We are too close to the edge, back up then continue
		// Set the output indicators to indicate this
		pixy.setLED(255, 255, 0);
		analogWrite(PIN_LED_GREEN, 255);

		// Set the encoder stop limits, then start reversing
		encoderLeftTarget = encoderLeft - 90;
		encoderRightTarget = encoderRight - 90;
		motor_drive(-SPEED_FAST);
		
		// Change states
		state = 31;
		reference_y -= 90;
		Serial5.println("[STS] State 31: Back away from perimiter");
		break;

	case 31:
		// Wait until we have reversed enough, then turn right a little one we have backed up enough
		if (encoderLeft < encoderLeftTarget && encoderRight < encoderRightTarget) {
			// We have reversed enough, so reset encoders ready to turn right
			motor_stop();
			encoderRightTarget = encoderRight - 250;
			motor_single_wheel(true, -SPEED_MED);

			pixy.setLED(255, 128, 0);
			state = 32;
		}
		break;

	case 32:
		// Once we have turned far enough, go back to our old stuff.
		if (encoderLeft < encoderLeftTarget) {
			motor_stop();
			state = 10;
			pixy.setLED(255, 0, 0);
		}
		break;

	case 50:
		// We have found a cube! Collect it!
		// Are we pointing (more or less) at the cube?
		pixy.setLED(0, 0, 255); /// Set the RGB LED Blue to indicate tracking a blue cube.
		if (pixyY > 5) {
			// We are "far away" from the block, so move forward
			// Set up some variables for motor speed. We want to motors to travel
			//  propotionally to how far away the block is off center.
			uint8_t speedLeft = SPEED_SLOW;
			uint8_t speedRight = SPEED_SLOW;

			if (pixyX > 0) speedLeft += pixyX;
			if (pixyX < 0) speedRight += abs(pixyX);

			motor_single_wheel(false, speedLeft);
			motor_single_wheel(true, speedRight);
		}
		else {
			// We have more or less arrived at the cube, so drive forward, just a little, just to be sure.
			motor_stop();
			state = 51;
			encoderLeftTarget = encoderLeft + 100;
			encoderRightTarget = encoderRight + 100;
			Serial5.println("[STS] State 51: Finish collecting cube");
		}
		break;

	case 51:
		// State 51: Drive forwards until cube is collected.
		if (encoderLeft > encoderLeftTarget && encoderRight > encoderRightTarget) {
			// We have driven far enough forward, so we will resume normal operation
			motor_stop();

			pixyColour = 0;
			state = 10;
		}
		break;

	case 90:
		// We are now returning to the base. Drive forward and keep the base centered.
		pixy.setLED(0, 255, 0); /// Set the RGB LED Green to indicate returning to base
		analogWrite(PIN_LED_GREEN, 128);
		analogWrite(PIN_LED_RED, 128);

		if (baseY > 5) {
			// We are "far away" from the base, so move forward
			// Set up some variables for motor speed. We want to motors to travel
			//  propotionally to how far away the block is off center.
			uint8_t speedLeft = SPEED_SLOW;
			uint8_t speedRight = SPEED_SLOW;

			if (baseX > 0) speedLeft += pixyX;
			if (baseX < 0) speedRight += abs(pixyX);

			motor_single_wheel(false, speedLeft);
			motor_single_wheel(true, speedRight);
		}
		else {
			// We have more or less arrived at the base, so drive forward, just a little, just to be sure.
			motor_stop();
			state = 91;

			encoderLeftTarget = encoderLeft + 100;
			encoderRightTarget = encoderRight + 100;
			Serial5.println("[STS] State 91: Returning to base");
		}
		break;

	case 91:
		// State 91: Drive towards the base, until we have arrived.
		if (encoderLeft > encoderLeftTarget && encoderRight > encoderRightTarget) {
			// We have probably arrived at the base
			motor_stop();
			pixyColour = 0;
			state = 100;
		}
		break;

	case 99:
		// State 999: We found a red block. Work out where it is and save its location
		Serial5.println("[CAM] ================");
		Serial5.println("[CAM] RED Block found!");
		Serial5.println("[CAM] ----------------");
		Serial5.print("[CAM] X: ");
		Serial5.print(red_x);
		Serial5.print("  Y: ");
		Serial5.println(red_y);
		break;

	case 100:
		// State 1000: We have finished the task.
		/// Set the lamps to indicate this fact
		pixy.setLamp(false, false);
		headlight = false;
		analogWrite(PIN_LED_GREEN, 255);
		analogWrite(PIN_LED_RED, 255);

		if ((millis() / 1000) % 2) {
			pixy.setLED(0, 255, 0);
		}
		else {
			pixy.setLED(255, 0, 0);
		}

		// If the button is pressed, reset for a fresh start.
		if (digitalRead(PIN_SWITCH) == LOW) {
			// The button has been pressed, reset to state one
			while (digitalRead(PIN_SWITCH) == LOW) {
				// Wait for the button to go be un-pressed...
			}
			state = 1;
		}
		break;

	default:
		// We are in an unknown state, and we don't know how we got here.
		// Return to state 0 with an error.
		Serial5.print("[ERR] Entered Unknown State: ");
		Serial5.println(state);
		Serial5.println("[ERR] I don't know how to handle that, returning to state zero");
		state = 0;
		break;
	}
}

// FUNCTION:    schedule_blocks
// INPUTS:      pixy array
// OUTPUTS:     state
// PURPOSE:     evaluates the input from the camera and decides if the state machine
//               needs to be jumped into another state.
void schedule_blocks() {
	// Do not do anything if we are already reacting to a block (or stopped)
	if (state < 900 && state > 10) {
		// We shall continue! We shall work out what to do!
		stateOld = state;
		switch (pixyColour) {
		case 1:
			// We are looking at a red block, so save it's location
			Serial5.println("[CAM] Found red, triangulating location.");
			state = 99;
			break;

		case 2:
			if ((millis() < 60000 + greenTime) && baseY < 50) {
				// We are looking at the green base area. We should probably finish there...
				state = 90;
				Serial5.println("[CAM] Found green, moving to base.");
			}
			break;

		case 3:
			// We are looking at a blue block, so start going to collect it
			state = 50;
			Serial5.print("[CAM] Gone to collect cube at location: ");
			Serial5.print(pixyX);
			Serial5.print(", ");
			Serial5.print(pixyY);
			Serial5.print(" and size ");
			Serial5.println((long int)pixy.ccc.blocks[pixyIndex].m_width * (long int)pixy.ccc.blocks[pixyIndex].m_height);
			break;
			
		default:
			// We will stay in the same state, because we don't have anything to do that is block related
			break;
		}
	}
}

// FUNCTION:    follow_line
// INPUTS:      isRight
// OUTPUTS:     motor_drive()
// PURPOSE:     Causes the robot to follow a line/edge on a given side of the robot.
// NOTES:       Input 'side' selects which side sensors to use:
//                false = left;
//                true = right.
void follow_line() {
	// Calculate how fast the opposite side should travel, compared to the constant side
	uint8_t value = SPEED_SLOW;
	if (irFlags[0]) {
		value -= 10;
	}
	else {
		value += 10;
	}

	motor_single_wheel(false, value);
	motor_single_wheel(true, SPEED_SLOW);
}

// FUNCTION:    sanity_check
// INPUTS:      millis()
// OUTPUTS:     LED_BUILTIN
// PURPOSE:     Toggles the state of the built-in LED every 500 ms as a sanity check.
void sanity_check() {
	if (millis() > timeOld + 500) {
		digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
		timeOld = millis();
	}
}

///////////////////////////////// EFFECTORS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// FUNCTION:    section_effector
// INPUTS:      nill
// OUTPUTS:     nill
// PURPOSE:     Enacts the wishes of the scheduler
void section_effector() {
	/// Print debug output if enabled
	if (debug) display_debug();
	/// Call any queued operations

	// Set the motor speeds to the given value
	analogWrite(PIN_MOTOR_LEFT_PWM, pid_motor_left(abs(PIDleft_setpoint), abs(speedLeft)));
	analogWrite(PIN_MOTOR_RIGHT_PWM, pid_motor_right(abs(PIDright_setpoint), abs(speedRight)));
}

// FUNCTION:    motor_drive
// INPUTS:      speed
// OUTPUTS:     motor pins
// PURPOSE:     Casues the robot to drive forwards or backwards (depending on
//                the sign of 'speed').
void motor_drive(int speed) {
	PIDleft_setpoint = speed;
	PIDright_setpoint = speed;
	// Set the H-Bridge to cause the motors to drive in the desired direction
	if (speed > 0) {
		// A positive value has been given, so we will drive forwards
		digitalWrite(PIN_MOTOR_LEFT_POW, HIGH);
		digitalWrite(PIN_MOTOR_LEFT_DIR, HIGH);
		digitalWrite(PIN_MOTOR_RIGHT_POW, HIGH);
		digitalWrite(PIN_MOTOR_RIGHT_DIR, HIGH);
	}
	else if (speed < 0) {
		// A negative value has been given, so we will drive backwards
		digitalWrite(PIN_MOTOR_LEFT_POW, HIGH);
		digitalWrite(PIN_MOTOR_LEFT_DIR, LOW);
		digitalWrite(PIN_MOTOR_RIGHT_POW, HIGH);
		digitalWrite(PIN_MOTOR_RIGHT_DIR, LOW);
	}
	else {
		// A value of zero has been passed to the function, so we should stop instead.
		motor_stop();
	}
}

// FUNCTION:    motor_rotate
// INPUTS:      speed (signed direction)
// OUTPUTS:     motor pins
// PURPOSE:     Causes the motors to drive in opposite directions with the
//                intention being that the robot will turn on the spot.
void motor_rotate(int speed) {
	PIDleft_setpoint = speed;
	PIDright_setpoint = speed;
	// Set the H-bridge to allow power to the motors
	if (speed > 0) {
		// A positive value has been given, so we will turn clockwise (right)
		digitalWrite(PIN_MOTOR_LEFT_POW, HIGH);
		digitalWrite(PIN_MOTOR_LEFT_DIR, HIGH);
		digitalWrite(PIN_MOTOR_RIGHT_POW, HIGH);
		digitalWrite(PIN_MOTOR_RIGHT_DIR, LOW);
	}
	else if (speed < 0) {
		// A negative value has been given, so we will turn anti-clockwise (left)
		digitalWrite(PIN_MOTOR_LEFT_POW, HIGH);
		digitalWrite(PIN_MOTOR_LEFT_DIR, LOW);
		digitalWrite(PIN_MOTOR_RIGHT_POW, HIGH);
		digitalWrite(PIN_MOTOR_RIGHT_DIR, HIGH);
	}
	else {
		// A value of zero has been passed to the function, so we should stop instead.
		motor_stop();
	}
}

// FUNCTION:    motor_single_wheel
// INPUTS:      wheel, speed (inc. direction)
// OUTPUTS:     motor pins
// PURPOSE:     Causes a single motor to run at a given speed (in a given
//                direction). Needed in case a single motor only needs to be
//                spun, or if it is desired for the robot to turn about a wheel
//                (rather than the center).
// NOTES:       Input 'Wheel' selects which side motor to use:
//                false = left;
//                true = right.
void motor_single_wheel(bool wheel, int speed) {
	if (wheel) {
		// RIGHT wheel has been selected
		PIDright_setpoint = speed;
		// Check if fowards or backwards, then set direction of output
		if (speed > 0) {
			// Forward movement
			digitalWrite(PIN_MOTOR_RIGHT_POW, HIGH);
			digitalWrite(PIN_MOTOR_RIGHT_DIR, HIGH);
		}
		else if (speed < 0) {
			// Backwards movement (reverse)
			digitalWrite(PIN_MOTOR_RIGHT_POW, HIGH);
			digitalWrite(PIN_MOTOR_RIGHT_DIR, LOW);
		}
		else {
			// We have been passed "0" as our speed, so we should probably stop.
			motor_stop();
		}
	}
	else {
		// LEFT wheel has been selected
		PIDleft_setpoint = speed;
		// Check if fowards or backwards, then set direction of output
		if (speed > 0) {
			// Forward movement
			digitalWrite(PIN_MOTOR_LEFT_POW, HIGH);
			digitalWrite(PIN_MOTOR_LEFT_DIR, HIGH);
		}
		else if (speed < 0) {
			// Backwards movement (reverse)
			digitalWrite(PIN_MOTOR_LEFT_POW, HIGH);
			digitalWrite(PIN_MOTOR_LEFT_DIR, LOW);
		}
		else {
			// We have been passed "0" as our speed, so we should probably stop.
			motor_stop();
		}
	}
}

// FUNCTION:    pid_motor_left, pid_motor_right
// INPUTS:      setpoint, input
// OUTPUTS:     output
// PURPOSE:     Given a target velocity, will adjust motor output to meet
//               that velocity.
uint8_t pid_motor_left(int16_t setpoint, int8_t input) {
	// Calculate the error values
	int errorP = setpoint - input;
	int errorD = errorP - PIDleft_lastError;
	PIDleft_lastError = errorP;

	// Calculate the output, based on the errors and output constants
	int output = ((errorP * PID_CONSTANT_P) + (PIDleft_errorI * PID_CONSTANT_I) + (errorD * PID_CONSTANT_D));

	// Do range checking on the output
	if (output > PID_OUTPUT_MAX) output = PID_OUTPUT_MAX;
	if (output < PID_OUTPUT_MIN) output = PID_OUTPUT_MIN;

	// Calculate the integral error
	PIDleft_errorI += errorP;

	// Do range checking on the integral error
	if (PIDleft_errorI > PID_OUTPUT_MAX) PIDleft_errorI = PID_OUTPUT_MAX;
	if (PIDleft_errorI < PID_OUTPUT_MIN) PIDleft_errorI = PID_OUTPUT_MIN;

	// Finally, return the output
	return output;
}
uint8_t pid_motor_right(int16_t setpoint, int8_t input) {
	// Calculate the error values
	int errorP = setpoint - input;
	int errorD = errorP - PIDright_lastError;
	PIDright_lastError = errorP;

	// Calculate the output, based on the errors and output constants
	int output = ((errorP * PID_CONSTANT_P) + (PIDright_errorI * PID_CONSTANT_I) + (errorD * PID_CONSTANT_D));

	// Do range checking on the output
	if (output > PID_OUTPUT_MAX) output = PID_OUTPUT_MAX;
	if (output < PID_OUTPUT_MIN) output = PID_OUTPUT_MIN;

	// Calculate the integral error
	PIDright_errorI += errorP;

	// Do range checking on the integral error
	if (PIDright_errorI > PID_OUTPUT_MAX) PIDright_errorI = PID_OUTPUT_MAX;
	if (PIDright_errorI < PID_OUTPUT_MIN) PIDright_errorI = PID_OUTPUT_MIN;

	// Finally, return the output
	return output;
}

// FUNCTION:    motor_stop
// INPUTS:      nill
// OUTPUTS:     motor pins
// PURPOSE:     Stops the motors moving
void motor_stop() {
	digitalWrite(PIN_MOTOR_LEFT_POW, LOW);
	digitalWrite(PIN_MOTOR_LEFT_DIR, LOW);
	digitalWrite(PIN_MOTOR_RIGHT_POW, LOW);
	digitalWrite(PIN_MOTOR_RIGHT_DIR, LOW);
}

// FUNCTION:    display_status
// INPUTS:      status information
// OUTPUTS:     serial1
// PURPOSE:     This takes basic information and sends it to the bluetooth
// FUNCTION:    serial interface to show what the machine is doing to an end user.
void display_status() {

}

// FUNCTION:    display_locations
// INPUTS:      block locations
// OUTPUTS:     serial1
// PURPOSE:     This takes the information on the location of red blocks, and
//                displays this information in an end-user friendly manner on
//                the bluetooth serial connection.
void display_locations() {

}

// FUNCTION:    display_pixy
// INPUTS:      nill
// OUTPUTS:     nill
// PURPOSE:     Prints debug information about the pixy.
void display_pixy() {
	Serial5.print("Currently tracking: ");
	Serial5.println(pixyIndex);
	Serial5.println(pixyColour);
	Serial5.print("Tracking: ");
	Serial5.println(pixy.ccc.numBlocks);
	Serial5.print("Target X: ");
	Serial5.println(pixyX);
	Serial5.print("Target Y: ");
	Serial5.println(pixyY);
	Serial5.print("Base X:   ");
	Serial5.println(baseX);
	Serial5.print("Base Y:   ");
	Serial5.println(baseY);
}

// FUNCTION:    display_debug
// INPUTS:      debug information
// OUTPUTS:     serial0
// PURPOSE:     This takes all information pertaining to the current status of
//                the robot with the aim of provinding useful information of the
//                current state to aid in the debugging the operation and
//                decisions of the robot.
void display_debug() {
	Serial5.print("[DBG] In state ");
	Serial5.println(state);
	Serial5.print("[DBG] previous state ");
	Serial5.println(stateOld);
	Serial5.print("[DBG] Encoder Left ");
	Serial5.println(encoderLeft);
	Serial5.print("[DBG] Encoder Right ");
	Serial5.println(encoderRight);
	Serial5.print("[DBG] IR ");
	Serial5.print(irFlags[0]);
	Serial5.print(":");
	Serial5.print(irFlags[1]);
	Serial5.print(":");
	Serial5.print(irFlags[2]);
	Serial5.print(":");
	Serial5.print(irFlags[3]);
	Serial5.print(":");
	Serial5.print(irFlags[4]);
	Serial5.print(":");
	Serial5.println(irFlags[5]);
}

///////////////////////////////// CONTROLLER \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// FUNCTION:    controller
// INPUTS:      nill
// OUTPUTS:     nill
// PURPOSE:     Creates an interactive CLI on the serial input(s) to allow
//                control of the robot, as well as debug output.
void controller() {
	/// HOW THIS WORKS: Normally stateCLI == 'null' == '\0'. In this state it
	///   continually checks Serial5.RX register for any new bytes. If there are,
	///   it sets that to the state and stops checking. Once whatever state is
	///   finished, it will reset stateCLI back to 'null'. It is managed this way
	///   to allow individual states to be interactive, without a 'read' statement
	///   outside the state machine here to override any of the states set by
	///   individual options in the menu.
	switch (stateCLI) {
	case 's':
	case 'S':
		// E-Stop
		motor_stop();
		stateOld = state;
		state = 0;
		Serial5.println("[CLI] Software E-Stop triggered!");
		Serial5.println("[CLI] Forced into state 0");
		stateCLI = '\0';
		break;

	case 'c':
		// CALIBRATE IR sensors
		Serial5.println("[CLI] Initialising black threshold for IR sensors");
		Serial5.println("[CLI] IR sensors should be on BLACK for calibration");
		initialise_ir(true);
		Serial5.println("[CLI] Initialised black threshold for IR sensors");
		stateCLI = '\0';
		break;

	case 'b':
		// drive BACKWARDS
		motor_drive(-Serial5.parseInt());
		stateCLI = '\0';
		break;

	case 'd':
		// DISPLAY a debug output
		display_debug();
		stateCLI = '\0';
		break;

	case 'D':
		// Toggle the DEBUG output
		debug = !debug;
		if (debug) {
			Serial5.println("[CLI] Debug output enabled");
		}
		else {
			Serial5.println("[CLI] Debug output disabled");
		}
		stateCLI = '\0';
		break;

	case 'f':
		// Drive forWard.
		motor_drive(Serial5.parseInt());
		stateCLI = '\0';
		break;

	case 'h':
		// Toggle HEADLIGHTS
		if (headlight)
		{
			pixy.setLamp(false, false);
			headlight = false;
		}
		else
		{
			pixy.setLamp(true, false);
			headlight = true;
		}
		stateCLI = '\0';
		break;

	case 'i':
		// Display IR output
		controller_ir();
		stateCLI = '\0';
		break;

	case 'j':
		// JUMP to state
		state = Serial5.parseInt();
		Serial5.print("[CLI] State changed to ");
		Serial5.println(state);
		stateCLI = '\0';
		break;

	case 'l':
		// rotate LEFT
		motor_rotate(-Serial5.parseInt());
		stateCLI = '\0';
		break;

	case 'L':
		// LEFT motor only
		motor_single_wheel(false, Serial5.parseInt());
		stateCLI = '\0';
		break;

	case 'o':
		// Print camera debug
		display_pixy();
		stateCLI = '\0';
		break;

	case 'p':
		// Display the current POSITION
		controller_location();
		stateCLI = '\0';
		break;

	case 'P':
		// Display debug from PIXY
		debugPixy = !debugPixy;
		if (debugPixy) {
			Serial5.println("[CLI] Pixy debug output enabled");
		}
		else {
			Serial5.println("[CLI] Pixy debug output disabled");
		}
		stateCLI = '\0';
		break;

	case 'Q':
		// Clears the pixy tracking database
		pixyIndex = '\0';
		stateCLI = '\0';
		break;

	case 'r':
		// Rotate RIGHT
		motor_rotate(Serial5.parseInt());
		stateCLI = '\0';
		break;

	case 'R':
		// LEFT motor only
		motor_single_wheel(true, Serial5.parseInt());
		stateCLI = '\0';
		break;

	case '?':
		// Print help info
		controller_help();
		stateCLI = '\0';
		break;

	case '\0':
	case '\n':
	case '\r':
		// Check if there is any new data on UART (serial) 5.
		if (Serial5.available()) {
			// Store the input from the serial into a variable to 'save' it.
			stateCLI = Serial5.read();
		}
		break;

	default:
		// Print "unknown command" screen
		Serial5.println("[ERR] That is not a command that I'm familiar with");
		Serial5.println("[CLI] Enter '?' for a list of all commands.");
		stateCLI = '\0';
		break;
	}
}

// FUNCTION:    controller_ir
// INPUTS:      nill
// OUTPUTS:     nill
// PURPOSE:     Prints a list of current IR values
void controller_ir() {
	Serial5.print("Front-Outer: ");
	Serial5.print(irFlags[0]);
	Serial5.print(" Raw: ");
	Serial5.println(irRaw[0]);
	Serial5.print("Front-Left:  ");
	Serial5.print(irFlags[1]);
	Serial5.print(" Raw: ");
	Serial5.println(irRaw[1]);
	Serial5.print("Front-Right: ");
	Serial5.print(irFlags[2]);
	Serial5.print(" Raw: ");
	Serial5.println(irRaw[2]);
	Serial5.print("Back-Outer:  ");
	Serial5.print(irFlags[3]);
	Serial5.print(" Raw: ");
	Serial5.println(irRaw[3]);
	Serial5.print("Back-Left:   ");
	Serial5.print(irFlags[4]);
	Serial5.print(" Raw: ");
	Serial5.println(irRaw[4]);
	Serial5.print("Back-Right:  ");
	Serial5.print(irFlags[5]);
	Serial5.print(" Raw: ");
	Serial5.println(irRaw[5]);
}

void controller_location() {
	//// TODO Print the current location of the robot to the bluetooth serial module.
}

// FUNCTION:    controller_help
// INPUTS:      nill
// OUTPUTS:     nill
// PURPOSE:     Prints a list of valid commands to the bluetooth serial interface
void controller_help() {
	Serial5.println("=================================");
	Serial5.println("ECD Smart Agriculture Competition");
	Serial5.println("Command Line Interface Options:  ");
	Serial5.println("=================================");
	Serial5.println("?    - Displays this help message");
	Serial5.println("S, s - Stops program running [E-Stop]");
	Serial5.println("");
	Serial5.println("b<v> - Reverse with given <velocity>");
	Serial5.println("c    - Calibrate the IR sensors");
	Serial5.println("d    - Displays a single debug output");
	Serial5.println("D    - Toggles auto debug output.");
	Serial5.println("f<v> - Drive forward at given <velocity>");
	Serial5.println("h    - Toggles the headlights");
	Serial5.println("i    - Display current IR status");
	Serial5.println("j<s> - Jumps to state <state>.");
	Serial5.println("l<v> - Rotate left with given <velocity>");
	Serial5.println("L<v> - Rotate left motor with given <velocity>");
	Serial5.println("o    - Displays Pixy parameters");
	Serial5.println("p    - Display current location/Position");
	Serial5.println("P    - Toggles auto debug information from pixy camera");
	Serial5.println("Q    - Reset the currently tracked pixy object");
	Serial5.println("r<v> - Rotate right with given <velocity>");
	Serial5.println("R<v> - Rotate right motor with given <velocity>");
	Serial5.println("");
	Serial5.println("");
}
